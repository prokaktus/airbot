from air.models import Location, GCReading
from rest_framework import serializers


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ('name', 'site_id',)


class GCReadingSerializer(serializers.HyperlinkedModelSerializer):
    compound = serializers.CharField(source='get_pollutant_display')

    class Meta:
        model = GCReading
        fields = ('pollutant', 'compound', 'value', 'date', 'location', 'created', 'id')


class ReadingThresholdSerializer(serializers.HyperlinkedModelSerializer):
    avg = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    pollutant = serializers.IntegerField()
    reading_count = serializers.IntegerField(read_only=True)
    sum = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)

    class Meta:
        model = Location
        fields = ('pollutant', 'site_id', 'name', 'avg', 'pollutant', 'reading_count', 'sum')