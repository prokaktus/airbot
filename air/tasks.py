import io
import requests
import csv
from decimal import Decimal, InvalidOperation
from datetime import datetime
from bs4 import BeautifulSoup
from django.apps import apps
from django.utils.timezone import make_aware
from airbot.celery import app


@app.task(max_retries=5)
def download_report(month=3, day=28, year=2016, site='48_201_0026', units='ppb-v'):
    """
    Expected format is a csv with variable length.

    The first column is the pollutant.
    Each column after that is the time from 0:00 -> 23:00 using 12 hour time

    :return:
    """

    include_map = {
        "include43202": "Ethane",
        "include43246": "2-Methyl-1-Pentene [P]",
        "include45209": "n-Propylbenzene [BP] *",
        "include43203": "Ethylene [P] *",
        "include43245": "1-Hexene [P]",
        "include45212": "m-Ethyltoluene [BP]",
        "include43204": "Propane [P] *",
        "include43231": "n-Hexane [BP] *",
        "include45213": "p-Ethyltoluene [BP]",
        "include43205": "Propylene [P] *",
        "include43289": "t-2-Hexene [BP]",
        "include45207": "1,3,5-Trimethylbenzene [BP] *",
        "include43214": "Isobutane [P] *",
        "include43290": "c-2-Hexene [BP]",
        "include45211": "o-ethyltoluene [BP]",
        "include43212": "n-Butane [P] *",
        "include43262": "Methylcyclopentane [BP] *",
        "include45208": "1,2,4-Trimethylbenzene [BP] *",
        "include43206": "Acetylene [P] *",
        "include43247": "2,4-Dimethylpentane [BP] *",
        "include43238": "n-Decane [BP] *",
        "include43216": "t-2-Butene [P] *",
        "include45201": "Benzene [BP] *",
        "include45225": "1,2,3-Trimethylbenzene [BP] *",
        "include43280": "1-Butene [P] *",
        "include43248": "Cyclohexane [BP] *",
        "include45218": "m-Diethylbenzene [BP]",
        "include43270": "Isobutene [P]",
        "include43263": "2-Methylhexane [BP] *",
        "include45219": "p-Diethylbenzene [BP]",
        "include43217": "c-2-Butene [P] *",
        "include43291": "2,3-Dimethylpentane [BP] *",
        "include43954": "n-Undecane [BP]",
        "include43242": "Cyclopentane [P] *",
        "include43249": "3-Methylhexane [BP] *",
        "include43256": "a-Pinene [BP]",
        "include43221": "Isopentane [P] *",
        "include43250": "2,2,4-Trimethylpentane [BP] *",
        "include43257": "b-Pinene [BP]",
        "include43220": "n-Pentane [P] *",
        "include43232": "n-Heptane [BP] *",
        "include43283": "Cyclopentene [P]",
        "include43218": "1,3-Butadiene [P] *",
        "include43261": "Methylcyclohexane [BP] *",
        "include43282": "3-Methyl-1-Butene [P]",
        "include43342": "3-Methyl-1-Butene+Cyclopentene [P]",
        "include43252": "2,3,4-Trimethylpentane [BP] *",
        "include61101": "Wind Speed",
        "include43226": "t-2-Pentene [P] *",
        "include45202": "Toluene [BP] *",
        "include61103": "Resultant Wind Speed",
        "include43228": "2-Methyl-2-Butene [P]",
        "include43960": "2-Methylheptane [BP] *",
        "include61104": "Resultant Wind Direction",
        "include43224": "1-Pentene [P] *",
        "include43253": "3-Methylheptane [BP] *",
        "include43227": "c-2-Pentene [P] *",
        "include43233": "n-Octane [BP] *",
        "include43244": "2,2-Dimethylbutane [P] *",
        "include45203": "Ethyl Benzene [BP] *",
        "include43284": "2,3-Dimethylbutane [P]",
        "include45109": "p-Xylene + m-Xylene [BP] *",
        "include43285": "2-Methylpentane [P]",
        "include45220": "Styrene [BP] *",
        "include43230": "3-Methylpentane [P]",
        "include45204": "o-Xylene [BP] *",
        "include43243": "Isoprene [P] *",
        "include43235": "n-Nonane [BP] *",
        "include43234": "4-Methyl-1-Pentene [P]",
        "include45210": "Isopropyl Benzene - Cumene [BP] *",
    }

    Location = apps.get_model('air', 'Location')
    GCReading = apps.get_model('air', 'GCReading')

    location = Location.objects.get(site_id=site)
    url = 'https://www.tceq.texas.gov/cgi-bin/compliance/monops/agc_daily_summary.pl'

    params = {
        'select_date': 'user',
        'user_month': month - 1,  # convert to 0 index
        'user_day': day,
        'user_year': year,
        'user_site': site,
        'user_units': units,
        'report_format': 'comma',
    }

    for key in include_map.keys():
        params[key] = 1

    response = requests.post(url, params)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        pre = soup.find('pre')
        data = io.StringIO()
        if pre.text[0] == '\n':
            data.write(pre.text[1:])
        else:
            data.write(pre.text[:])
        data.seek(0)

        reader = csv.reader(data)

        header = None
        for row in reader:
            if header is None:
                header = row
                continue
            try:
                pollutant = GCReading.lookup(row[0])
            except IndexError:
                continue
            if pollutant == -1:
                continue

            for i, (title, cell) in enumerate(zip(header, row)):
                if i == 0:
                    continue

                date = datetime(month=month, day=day, year=year, hour=i - 1, minute=0, second=0)
                date = make_aware(date, timezone=GCReading.TZ)

                try:
                    value = Decimal(cell)
                except InvalidOperation:
                    if cell == "ST":
                        value = GCReading.ST
                    elif cell == "BL":
                        value = GCReading.BL
                    else:
                        continue
                reading, created = GCReading.objects.get_or_create(location=location, date=date, pollutant=pollutant,
                                                                   value=value)

    else:
        num_retries = download_report.request.retries
        seconds_to_wait = 2.0 ** num_retries

        # First countdown will be 1.0, then 2.0, 4.0, etc.
        raise download_report.retry(countdown=seconds_to_wait)