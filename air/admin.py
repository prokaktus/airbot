from django.contrib import admin
from air.models import Location, GCReading, POLLUTANT_CHOICES
from django.contrib.admin import SimpleListFilter
from django.utils.timezone import localtime


class GCReadingPollutantFilter(SimpleListFilter):
    title = 'Pollutant'
    parameter_name = 'pollutant'

    def lookups(self, request, model_admin):
        return POLLUTANT_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(pollutant=self.value())
        return queryset


class GCReadingAdmin(admin.ModelAdmin):
    list_display = ('location', 'time', 'pollutant', 'pvalue')
    list_filter = [GCReadingPollutantFilter, ('location', admin.RelatedOnlyFieldListFilter)]

    def time(self, instance):
        cst = localtime(instance.date, GCReading.TZ)
        return cst.strftime("%m/%d/%Y - %I %p")

    def pvalue(self, instance):
        if instance.value == GCReading.ST:
            return "ST"
        elif instance.value == GCReading.BL:
            return "BL"
        return instance.value


class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'site_id')


admin.site.register(Location, LocationAdmin)
admin.site.register(GCReading, GCReadingAdmin)
