from rest_framework import viewsets
from django.db.models import Avg, F, Q, Sum, Count
from django.utils.timezone import now, timedelta, datetime
from air.models import Location, GCReading
from air.serializers import LocationSerializer, GCReadingSerializer, ReadingThresholdSerializer


class LocationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows locations to be viewed.
    """
    queryset = Location.objects.all().order_by('name')
    serializer_class = LocationSerializer


class GCReadingViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows readings to be viewed.
    """
    queryset = GCReading.objects.all()
    serializer_class = GCReadingSerializer


class ReadingThresholdsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that averages the last 24 hours of each pollutant at each location
    """
    queryset = Location.objects.all()
    serializer_class = ReadingThresholdSerializer

    def get_queryset(self):
        query = self.queryset

        timestamp = self.request.query_params.get('date', None)
        if timestamp is not None:
            timestamp = datetime.fromtimestamp(int(timestamp))
        else:
            timestamp = now() - timedelta(days=1)

        try:
            pollutant = int(self.request.query_params.get('pollutant', None))
        except TypeError:
            pollutant = F('gcreading__pollutant')

        site_id = self.request.query_params.get('site_id', None)
        if site_id is not None:
            if Location.objects.filter(site_id=site_id).exists():
                query = query.filter(site_id=site_id)
            else:
                query = query.none()

        # Only give us readings > 0 as error codes / statuses are always less than 0
        query = query.filter(gcreading__date__gte=timestamp,
                             gcreading__value__gte='0.0',
                             gcreading__pollutant=pollutant)
        # do our calculations
        query = query.annotate(avg=Avg('gcreading__value'),
                               pollutant=F('gcreading__pollutant'),
                               reading_count=Count('gcreading__pk'),
                               sum=Sum('gcreading__value'))
        query = query.values().order_by('id', 'pollutant')
        return query
