from django.core.management.base import BaseCommand
from django.utils.timezone import now
import datetime
from air.tasks import download_report
from air.models import Location


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        date = now() - datetime.timedelta(hours=6)
        parser.add_argument('--year',
                            dest='year',
                            default=date.year,
                            type=int,
                            help='The year to download')

        parser.add_argument('--month',
                            dest='month',
                            default=date.month,
                            type=int,
                            help='The month to download')

        parser.add_argument('--day',
                            dest='day',
                            default=date.day,
                            type=int,
                            help='The day to download')

        parser.add_argument('--site_id',
                            dest='site_id',
                            default=None,
                            help='The id of the site where you want to download')

    def handle(self, *args, **options):
        # if 'year' in options or 'month' in options or 'day' in options:
        date = datetime.datetime(month=options['month'],
                                 day=options['day'],
                                 year=options['year'],)

        # print("%s " % options)
        site_id = options['site_id']
        locations = Location.objects.all()

        self.stdout.write("Downloading report for %s" % date.strftime("%m/%d/%Y"))
        if site_id is not None:
            locations = Location.objects.filter(site_id=site_id)

        for location in locations:
            download_report.delay(month=date.month, day=date.day, year=date.year, site=location.site_id)
            self.stdout.write("Queued up %s" % location.name)
