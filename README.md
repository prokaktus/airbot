# Airbot

Airbot is a Python/Django app to that aims to gather the first air quality violation dataset. It consists of of a scraper,
to get data from the Texas Commission on Environmental Quality (TCEQ) and a REST API.

## Install

Development of Airbot requires Python3 and virtualenv. 

First setup your virtualenv - this allows us to install Airbot and it's requirements on a self-contained manner.

    virtualenv --python=python3 ~/airbot
    
Next, activate your virtualenv.

    source ~/airbot/bin/activate
   
Install the dependencies

    pip install -r airbot/requirements.txt 
    
## First Run

There are two parts Airbot: the API server and Scraper.

You chould create postgres database named `airbot`. If you already have installed postgresql, then you can run:

    createdb airbot

Then you should run python commands:

    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver

You should then by able to login by visiting [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)


## Scraping Data

With your virtualenv activated you can download today's reports by running the following django management command:

    python manage.py scrape_data

This will download all data for all locations. If you'd like to download just a single location's readings, use the site_id argument.
Site Ids can be found in  [http://127.0.0.1:8000/admin/air/location](http://127.0.0.1:8000/admin/air/location)

    python manage.py scrape_data --site_id 48_355_0083

Scrape data can also take the following command line arguments to download historical data:
    
    --year YEAR           The year to download
    --month MONTH         The month to download
    --day DAY             The day to download
    --site_id SITE_ID     The id of the site where you want to download
 
## API

The API currently requires users to be authenticated and to pass in the credentials as part of the request. Explore it
by visiting [http://127.0.0.1:8000/](http://127.0.0.1:8000/). Under heavy development, so expect breakage.

